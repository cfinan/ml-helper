# ml-helper

## The ml-helper
This provides a template python package with all the various attributes implemented. You will need to change some names and references in various files. This is documented in `./docs/source/ml_helper_overview.rst`. There is a bash script that will perform some of the initialisation for you. The steps are:

1. Create your repository using the GitLab web interface
2. Clone it locally and cd into the root dir.
3. Create a dev branch: `git checkout -b dev`
4. Download [`init-package.sh`](https://gitlab.com/cfinan/ml-helper/-/raw/master/resources/bin/init-package.sh). Make it executable with `chmod +x init-package.sh`. To see the help just run `./init-package.sh` with no arguments.
5. Run `init-package.sh` . So If you have a package called my-package and you want to import it as `my_package`. Then you would run `init-package my-package my_package /path/to/my-package`. This will clone the skeleton package and change various references.
6. Delete `init-package.sh`

After that there is a manual checklist below, that you can use to customise package specific settings:

### Post setup checklist
1. Copy/create any module code over into the repository code directories. If your package is called my-package. Then the code directory will be at `/path/to/my-package/my_package`.
2. Delete or use the template python script. If your package is called my-package. Then the Python script will be at `/path/to/my-package/my_package/my_package.py`.
3. Delete the log module if not needed. If your package is called my-package. Then the Python script will be at `/path/to/my-package/my_package/log.py`.
3. Delete or populate the dummy orm file. If your package is called my-package. Then the it will be at `/path/to/my-package/my_package/orm.py`.
4. `setup.py`
   * Check the entry points are valid, add any you need. You **WILL** need to delete the template entry point. `skeleton-cmd-line`
   * Check the trove classifiers are valid.
   * Check external modules are added (i.e example data)
   * Update description, e-mail address etc...
5.  Check that the `MANIFEST.in` incorporates the example data
6.  Check that `LICENSE.txt` matches classifiers in `setup.py`
7.  Check the root `README.md` is correct/updated
   * Basic install options
   * Caveats on development/testing versions
8.  Check `.bumpversion.cfg` is correct/updated
9. Check `.gitignore` is correct/updated
10. Check `contributors.txt` is correct/updated
11. Check `requirements.txt` is correct/updated, adding in requirements for my non pypi git packages
12. Check `.gitlab-ci.yml` is correct/updated
13. Check `./resources/conda/envs` matches `requirements.txt`
14. Check `./resources/conda/build` matches `requirements.txt`
15. Check `./docs/source/conf.py` is correct/updated
16. Remove any unused references from the docs or update as needed:
    * Check that API documentation references the relevant parts of the API
    * Command line programs.
    * Examples: `./source/examples.rst` `./source/examples/*.nblink`
    * Data dicts: `./source/data_dict`,
    * Remove/update schema references `./source/schema.rst` and the index entry, if not using a database.
    * Update the repo root/contact E-mail in `./docs/source/contributing.rst`
17. Delete `./resources/docker` directory is it exists.
18. Check that any `./resources/bin` scripts and `./resources/task` scripts are added to the docs, and `hpc-tools` is added to requirements if task scripts are present.
19. Adjust the pytests
   * Add modules to the import tests
   * comment out template tests
20. Check that `./resources/ci_cd` scripts are valid
   * The bash scripts are executable
   * Any ORM components are covered by the build
   * Images are copied over
   * Any data dictionaries are copied over
   * The `README.md` references appropriate docker image (and matches `.gitlab-ci.yml`)
   * pytests are executed
21. Make sure test build directory exists in `~/pCloudDrive/Public Folder/` or where ever you want test docs to be built.
22. Make sure `~/pCloudDrive/Public Folder/index.html` has the correct links
23. Perform a `./resources/ci_cd` test build, fix any broken stuff
24. Commit any changes to dev
25. Increment version if needed
26. checkout master
27. merge dev to master and commit, push
28. build conda packages

You will want to delete up to this paragraph for your own package but keep and modify everything below.

__version__: `0.1.0a0`

The ml-helper package is a toolkit to......

There is [online](https://cfinan.gitlab.io/ml-helper/index.html) documentation for ml-helper.

## Installation instructions
At present, ml-helper is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that you install in either of the two ways below.

### Installation using conda
I maintain a conda package in my personal conda channel. To install from this please run:

```
conda install -c cfin -c bioconda -c conda-forge ml-helper
```

There are currently builds for Python v3.8, v3.9, v3.10 for Linux-64 and Mac-osX.

Please keep in mind that all development is carried out on Linux-64 and Python v3.8/v3.9. I do not own a Mac so can't test on one, the conda build does run some import tests but that is it.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/ml-helper.git
cd ml-helper
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/envs` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9, v3.10.

## Run the tests
After installation you will be able to run the tests. If you have cloned the repository you can run the command below from the root of the repository:

1. Run the tests using ``pytest ./tests``

## Installing the webdrivers ##
Some of the scripts use Selenium. This requires a [browser driver](https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/) to be installed (webdriver). For Firefox this is referred to as [`geckodriver`](https://github.com/mozilla/geckodriver/releases). The driver's location will need to be in your PATH in your `~/.bashrc` or `~/.bash_profile` and should be matched to your browser version.

The automated usage of [Google Chrome](https://www.google.co.uk/chrome/) (or chromium if you prefer) requires that the corresponding [webdriver](https://chromedriver.chromium.org/downloads) must be available. The webdriver version must be matched to your chrome/chromium version. You will need webdrivers for both Chrome and Firefox.

## Command endpoints ##

### Python scripts
ml-helper contains a lot of command line endpoints that are installed along with the package. Documentation for these can be found [here](https://cfinan.gitlab.io/ml-helper/scripts.html#python-scripts).

### BASH scripts
In addition to the Python command-line scripts that are available when the package is installed. There are also some bash scripts located in ``./resources/bin``. Please note these will not be installed when you install via clone & pip or a conda install. If using conda you will have to clone the repo. With either install method you will need to add the ``./resources/bin`` directory to your PATH.

These scripts will require two bash libraries to be in your PATH.

1. ``shflags`` - `This <https://github.com/kward/shflags>`_ is to manage bash command line arguments.
2. ``bash-helpers`` - `This <https://gitlab.com/cfinan/bash-helpers>`_ wraps some handle bash functions.

For more information on what is available see the [bash script](https://cfinan.gitlab.io/ml-helper/scripts.html#bash-scripts) documentation.

## Database configuration
The package uses SQLAlchemy to handle database interaction. This means that in theory you are not restricted to a particular database backend. In practice most testing/development will be performed against SQLite and MySQL. So, if you use something else and run into issues, please submit an issue or get in contact.

Any database connection parameters can be supplied on the command line or via a configuration file. You can supply a full connection URL directly on the command line. However, this is not a good idea if your database is password protected. In this case you should supply the connection parameters in a connection file `.ini` file. They should be set out as below:

```
[my_db_sqlite_conn]
# An SQLAlchemy connection URL, see:
# https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config
# All connection options here should be prefixed with umls
# Make sure passwords are URL escaped:
# import urllib.parse
# PW = urllib.parse.quote_plus(PW)
# Also, don't forget to escape any % that are in the URL (with a second %)
db.url = sqlite:////data/my_db.db

[my_db_mysql_conn]
db.url = mysql+pymysql://user:password@hostname/my_db

[my_db_postgres_conn]
db.url = postgresql+psycopg2://://user:password@hostname/my_db
```

Then to use these from the command line you can supply the section header from the config file to the script, so ``my_db_mysql_conn`` for the connection to the MySQL database.

### Change log ###

#### version `0.1.1a0` ####

