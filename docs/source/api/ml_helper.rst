``skeleton_package`` package
============================

``skeleton_package.skeleton_module`` module
-------------------------------------------

.. automodule:: skeleton_package.skeleton_module
   :members:
   :undoc-members:
   :show-inheritance:

..
   Some less used options
   :private-members:
   :inherited-members:
   :undoc-members:
   :show-inheritance:
   :member-order: bysource

..
   Can reference functions in API docs like this
.. autofunction:: skeleton_package.skeleton_module.run_stupid_function

..
   Or like this
.. currentmodule:: skeleton_package.skeleton_module
.. autofunction:: docstring_illustration
