Example code
============

Here are some examples of using the API. For examples of using scripts please see the :ref:`command-line <cmd_line>` section.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   examples/using_example_data
