.. ml-helper documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ml-helper
===========================

Description...

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   
   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Example code

   examples

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   schema
   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
