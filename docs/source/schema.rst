===============
Database schema
===============

The schema used by....

Below is a diagram of the currently implemented schema with the ``column`` links between the tables shown in green.

.. image:: ./_static/db-schema.png
   :width: 500
   :alt: db-schema schema
   :align: center

.. include:: ./data_dict/db-schema-tables.rst
