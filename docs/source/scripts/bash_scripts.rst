===========================
``./resourses/bin`` scripts
===========================

.. _tag_to_link:

``bash-script.sh``
~~~~~~~~~~~~~~~~~~

This performs...:

.. code-block:: console

   $ bash-script.sh --help
   USAGE: bash-script.sh [flags] TBC...
   flags:
     -v,--[no]verbose:  give more output (default: false)
     -h,--help:  show this help (default: false)
