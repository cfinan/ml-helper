==============
Python Scripts
==============

.. _stupid_program:

``stupid-cmd-line-program``
--------------------------

.. argparse::
   :module: skeleton_package.skeleton_module
   :func: _init_cmd_args
   :prog: stupid-cmd-line-program

Example usage
~~~~~~~~~~~~~

The script can be run as below

.. code-block::

   stupid-cmd-line-program 1 34


Output columns
~~~~~~~~~~~~~~

This script is designed to... The output columns are shown below.

..
   I include column descriptions like this, these are generated at
   build time from files in the ./resources/data_dict directory
.. include:: ../data_dict/stupid-cmd-line-program.rst

Known Issues
~~~~~~~~~~~~

This script is pointless.
