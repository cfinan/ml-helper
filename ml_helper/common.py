"""Some common functions the perform lots of the tasks you do before
running a model
"""
from datasets import load_dataset as ld
from pyaddons import utils
from torch import cuda
import csv
import os


TRAIN_DATA_NAME = "train"
"""The name for the training dataset (`str`)
"""
VALIDATE_DATA_NAME = "validate"
"""The name for the validation dataset (`str`)
"""
TEST_DATA_NAME = "test"
"""The name for the test dataset (`str`)
"""
CUDA_DEVICE_NAME = "cuda"
"""The name for a CUDA device (`str`)
"""
CPU_DEVICE_NAME = "cpu"
"""The name for a CPU device (`str`)
"""
HF_TOKEN_NAME = "HF_TOKEN"
"""The name for the huggingface environment variable (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_labels(label_file, int_label_col="label", text_label_col="desc",
                delimiter="\t"):
    """Get the mappings between the text labels and the integer labels
    required by the model.

    This can accept either a csv file with an integer label and text label
    column or a list of text labels, one per line.

    Parameters
    ----------
    label_file : `str`
        The path to the label file, it is assumed that it is GZIP compressed.
        It should be tab delimited with 3 columns, the maodel 0-based integer
        label, the BNF chapter code and the BNF chapter description.
    int_label_col : `str` or `NoneType`, optional, default: `label`
        The name of the integer label column. Set to NoneType if you have a
        single label tow per line.
    text_label_col : `str` or `NoneType`, optional, default: `label`
        The name of the text label column. Set to NoneType if you have a
        single label tow per line.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the file if column based.

    Returns
    -------
    labels_to_id : `dict`
        The labels to use, the keys are model integers (0-based) and the
        values are text labels.
    id_to_labels : `dict`
        The labels to use, the keys are text labels and the values are model
        integers (0-based).

    Notes
    -----
    Both the int_label_col and the text_label_col need to be None in order to
    read in as a list file.
    """
    open_method = utils.get_open_method(label_file)
    labels = dict()

    with open_method(label_file, 'rt') as infile:
        if int_label_col is None and text_label_col is None:
            for idx, row in enumerate(infile):
                labels[idx] = row.strip()
        else:
            reader = csv.DictReader(infile, delimiter="\t")
            for row in reader:
                labels[int(row[int_label_col])] = row[text_label_col]
        return labels, {label: id for id, label in labels.items()}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_tokenize_function(tokenizer, text_field="text"):
    """A tokenisation function, that will take an input row dictionary and
    return the tokenised text.

    Parameters
    ----------
    tokenizer : ``
        The tokeniser to use.
    text_field : `str`, optional, default: `text`
        The name of the text field.

    Returns
    -------
    tokeniser_func : `function`
        A tokeniser function.
    """
    def _tokeniser(row):
        return tokenizer(row[text_field], truncation=True)
    return _tokeniser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_dataset(datapath, prefix, delimiter="\t", train=True, test=True,
                 validate=True, dtype="csv"):
    """Load a train, validate, test dataset.

    Parameters
    ----------
    datapath : `str`
        The base directory where your input files are located.
    prefix : `str`
        A format string for the input file names. It is expected that they have
        the same name structure with the exception of the keywords, train,
        test, validate. So, for example: ``my_data_train.txt.gz``,
        ``my_data_validate.txt.gz`` and ``my_data_test.txt.gz``. These would
        be represented by ``my_data_{0}.txt.gz``.
    train : `bool`, optional, default: `True`
        Should the training dataset be loaded.
    validate : `bool`, optional, default: `True`
        Should the validate dataset be loaded.
    test : `bool`, optional, default: `True`
        Should the test dataset be loaded.
    dtype : `str`, optional, default: `csv`
        The datatype that is being loaded.

    Returns
    -------
    dataset : ``
        The loaded dataset.

    Raises
    ------
    ValueError
        If no data sets are loaded, this will happen when train, test, validate
        are set to False.
    """
    base_name = os.path.join(datapath, prefix)
    data_files = {}

    if train is True:
        data_files[TRAIN_DATA_NAME] = base_name.format(TRAIN_DATA_NAME)

    if validate is True:
        data_files[VALIDATE_DATA_NAME] = base_name.format(VALIDATE_DATA_NAME)

    if test is True:
        data_files[TEST_DATA_NAME] = base_name.format(TEST_DATA_NAME)

    if len(data_files) == 0:
        raise ValueError(
            "No data specified in data files. Ensure one of train, validate"
            " or test is true"
        )

    return ld(dtype, data_files=data_files, delimiter=delimiter)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_device_map():
    """Get the device to run the ML models on. This tests for CUDA availability
    and defaults to CPU if not available.

    Returns
    -------
    device_name : `str`
        Either ``cuda`` or ``cpu``.
    """
    return CUDA_DEVICE_NAME if cuda.is_available() else CPU_DEVICE_NAME


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_label_values(label_dict):
    """Get the values from a label mapping dictionary ordered by the label
    index.

    Parameters
    ----------
    label_dict : `dict`
        The keys are label index values used in models and the values are text
        labels or some other value associated with the index labels.

    Returns
    -------
    ordered_values : `list`
        The values ordered by the index keys.
    """
    return [j for i, j in sorted(label_dict.items(), key=lambda x: x[0])]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_class_weights(inpath, label_col="labels", delimiter="\t"):
    """Get the class weights. This loads the data file and gets the weights
    of the classes, these can be used in weighted cross entropy loss.

    Parameters
    ----------
    inpath : `str`
        The file containing the data and labels (classes).
    label_col : `str`, optional, default: `labels`
        The name of the label column in the input file.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the input file.

    Returns
    -------
    weights : `dict`
        The keys are the class indexes (`int`) and the values are the weights
        for each class.
    """
    counts = get_class_numbers(inpath, label_col=label_col,
                               delimiter=delimiter)
    n = len(counts)
    total = sum(counts.values())

    for i in counts.keys():
        counts[i] = total / (n*counts[i])
    return counts


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_class_numbers(inpath, label_col="labels", delimiter="\t"):
    """Get the class counts. This loads the data file and gets the counts of
    each class.

    Parameters
    ----------
    inpath : `str`
        The file containing the data and labels (classes).
    label_col : `str`, optional, default: `labels`
        The name of the label column in the input file.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the input file.

    Returns
    -------
    counts : `dict`
        The keys are the class indexes (`int`) and the values are the counts
        for each class.
    """
    open_method = utils.get_open_method(inpath)
    counts = dict()
    with open_method(inpath, 'rt') as infile:
        reader = csv.DictReader(infile, delimiter=delimiter)
        for row in reader:
            label = int(row[label_col])
            try:
                counts[label] += 1
            except KeyError:
                counts[label] = 1
    return counts


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_hf_token():
    """Get the huggingface token if available.

    Returns
    -------
    token : `str` or `NoneType`
        The huggingface token or nothing if not available.
    """
    try:
        return os.environ['HF_TOKEN']
    except KeyError:
        return None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def toggle_tokenise_parallel():
    """Toggle the tokeniser parallel environment variable. This is used to
    remove the warning about forking.

    Returns
    -------
    current_state : `bool`
        The state the ``TOKENIZERS_PARALLELISM`` was changed to.

    Notes
    -----
    Attempt to stop this warning: huggingface/tokenizers: The current
    process just got forked, after parallelism has already been used.
    Disabling parallelism to avoid deadlocks...
    To disable this warning, you can either:
             - Avoid using `tokenizers` before the fork if possible
             - Explicitly set the environment variable
      TOKENIZERS_PARALLELISM=(true | false)

    I have no idea what is causing this.
    """
    bool_map = dict(true=True, false=False)
    map_bool = {True: "true", False: "false"}

    os.environ['TOKENIZERS_PARALLELISM'] = 'false'
    try:
        current = bool_map[os.environ['TOKENIZERS_PARALLELISM']]
    except KeyError:
        current = True

    new = not current
    os.environ['TOKENIZERS_PARALLELISM'] = map_bool[new]
    return new
