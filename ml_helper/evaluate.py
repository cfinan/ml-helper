"""Evaluation classes.
"""
from sklearn.metrics import (
    accuracy_score,
    precision_score,
    recall_score,
    f1_score,
    confusion_matrix,
    classification_report
)
import numpy as np
import pandas as pd
import os
import re
import torch
import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class EvaluateBase(object):
    """A base evaluation class, this is to wrap simple/basic evaluation metrics
    for an easy interface.

    Parameters
    ----------
    model : `transformers.PreTrainedModel`
        The model that is being evaluated. This is only used to generate output
        paths.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, model):
        self.model = model

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def _get_predictions_from_logits(cls, logits):
        """Get class prediction labels from logit, this assumes a single label
        prediction but can be multi-class.

        Parameters
        ----------
        logits : `np.ndarray`
            A numpy array of logits the length is the number of samples, the
            width is the number of classes. The max logit is returned for each.

        Returns
        -------
        max_logits : `np.ndarray`
            This will have the class index having the maximum logit value for
            each sample.
        """
        return np.argmax(logits, axis=-1)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def _get_metrics(cls, true_labels, logits, prec_kwargs=None,
                     recall_kwargs=None, f1_kwargs=None):
        """Compute the basic metrics.

        Parameters
        ----------
        true_labels : `numpy.ndarray`
            The actual labels. This is a vector with length nsamples.
        logits : `np.ndarray`
            A numpy array of logits the length is the number of samples, the
            width is the number of classes. The max logit is returned for each.
        prec_kwargs : `dict`, optional, default : `None`
            Keyword arguments for the scipy precision function.
        recall_kwargs : `dict`, optional, default : `None`
            Keyword arguments for the scipy recall function.
        f1_kwargs : `dict`, optional, default : `None`
            Keyword arguments for the scipy f1_score function.

        Returns
        -------
        preds : `np.ndarray`
            This will have the predicted class for each input.
        accuracy : `float`
            The model accuracy.
        precision : `float`
            The model precision.
        recall : `float`
            The model recall.
        f1 : `float`
            The model F1.
        """
        prec_kwargs = prec_kwargs or {}
        recall_kwargs = recall_kwargs or {}
        f1_kwargs = f1_kwargs or {}
        preds = cls._get_predictions_from_logits(logits)
        accuracy = accuracy_score(true_labels, preds)
        precision = precision_score(true_labels, preds, **prec_kwargs)
        recall = recall_score(true_labels, preds, **recall_kwargs)
        f1 = f1_score(true_labels, preds, **f1_kwargs)
        return preds, accuracy, precision, recall, f1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def compute_metrics(self, pred):
        """Compute the basic metrics. This is designed to be used in a
        training run.

        Parameters
        ----------
        pred : `EvalPred`
            The prediction object from the trainer.

        Returns
        -------
        metrics : `dict`
            The metric dictionary. Has the accuracy, precision, recall,
            and F1.
        """
        preds, accuracy, precision, recall, f1 = self._get_metrics(
            pred.label_ids, pred.predictions
        )

        return dict(
            accuracy=accuracy,
            precision=precision,
            recall=recall,
            f1=f1,
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def evaluate_metrics(self, pred):
        """Compute the evaluation metrics. This is designed to be used in an
        evaluation run.

        Parameters
        ----------
        pred : `EvalPred`
            The prediction object from the trainer.

        Returns
        -------
        metrics : `dict`
            The metric dictionary. Has the accuracy, precision, recall,
            and F1. Also included is the confusion matrix and a classification
            report.
        """
        labels = pred.label_ids
        preds, accuracy, precision, recall, f1 = self._get_metrics(
            labels, pred.predictions
        )
        # Create confusion matrix
        conf_matrix = confusion_matrix(labels, preds)
        report = classification_report(
            labels, preds, digits=4
        )

        return dict(
            accuracy=accuracy,
            precision=precision,
            recall=recall,
            f1=f1,
            confusion_matrix=conf_matrix,
            classification_report=report
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def evaluate_model(self, trainer, outpath, prefix):
        """Perform evaluation on the model.

        Parameters
        ----------
        trainer. : `transformers.Trainer`
            The trainer.
        outpath : `str`
            An output directory to write the metrics.
        prefix : `str`
            A prefix for the fie names.

        Returns
        -------
        metrics : `dict`
            The metric dictionary. Has the accuracy, precision, recall,
            and F1. Also included is the confusion matrix and a classification
            report.

        Notes
        -----
        In addition to returning the metrics it will write them to file and
        produce a confusion matrix plot.
        """
        eval_outpath = os.path.join(outpath, prefix)

        if not os.path.isdir(outpath):
            raise NotADirectoryError(
                f"Output path does not exist or is not a directory: {outpath}"
            )
        confusion_data = f"{eval_outpath}-confusion.txt"
        confusion_plot = f"{eval_outpath}-confusion.png"
        class_report = f"{eval_outpath}-report.txt"
        metrics = trainer.evaluate()

        with open(class_report, "wt") as outfile:
            outfile.write(metrics['eval_classification_report'])

        class_labels = [
            trainer.model.config.id2label[i] for i in sorted(
                trainer.model.config.id2label.keys()
            )
        ]

        data = pd.DataFrame(
            metrics['eval_confusion_matrix'],
            columns=class_labels, index=class_labels
        )
        data.to_csv(
            confusion_data, sep="\t", index=True, header=True
        )
        return metrics

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_outpath(self):
        """Get the output path and prefix from the model.

        Returns
        -------
        outdir : `str`
            The directory that the model directory is located in.
        model_name : `str`
            The base name of the model directory.

        Raises
        ------
        NotADirectoryError
            If the model name is not a directory.
        """
        model_name = self.model.name_or_path
        if not os.path.isdir(model_name):
            raise NotADirectoryError(
                "Model name is not a directory"
            )

        outdir = os.path.dirname(model_name)
        model_name = re.sub(r'.model$', '', os.path.basename(model_name))
        return outdir, model_name


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class EvaluateBinaryClassifier(EvaluateBase):
    pass
