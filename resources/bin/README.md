# bin directory
This is used for non-python cmd-line programs such as bash scripts that are associated with the package. There may (will) be a better way of doing this that I have not come across yet. This directory should be added to your PATH in your `~/.bashrc` or `~/.bash_profile`.
