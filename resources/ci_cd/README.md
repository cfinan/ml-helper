# Continuous integration scripts
This allows the testing of various continuous integration scripts before committing to GitLab. It is a major pain in the backside to debug against that so it is easier to do a test run before setting it up.

The various `run_<pipeline>.sh` scripts represent different scripts that can be run on different pipelines. The other scripts represent small sections that can be run in a pipeline.

To run a test build prior to committing:

```
# ./run_docker.sh <docker image> <repo root> <doc build dir>
./run_docker.sh chrisfin/pypan:complete ~/code/snomed-ct /path/to/doc/build/directory
```

Other options for the docker image are:

`chrisfin/pypan:complete`
`chrisfin/pypan:data_sci`
`chrisfin/pypan:base`
`chrisfin/bio_misc:build`
`chrisfin/merit:master`
`chrisfin/skyline:build`
